# Picture and Notation schemas

# --- !Ups


CREATE TABLE users (
    user_id serial PRIMARY KEY,
    name varchar(30) NOT NULL,
    email varchar(60) NULL,
    password varchar(20) NOT NULL,
    registered timestamp NOT NULL
);

CREATE TABLE pictures (
    pic_id serial PRIMARY KEY,
    user_id integer NOT NULL REFERENCES users,
    unique_name varchar(20) NOT NULL,
    name varchar(60) NOT NULL,
    description text NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    format varchar(10) NULL,
    uploaded timestamp NOT NULL
);

CREATE TABLE notations (
    notation_id serial PRIMARY KEY,
    pic_id integer REFERENCES pictures,
    user_id integer NOT NULL REFERENCES users,
    xcoord smallint NOT NULL,
    ycoord smallint NOT NULL,
    comment text NOT NULL,
    created timestamp NOT NULL
);

CREATE TABLE tags (
    tag_id serial PRIMARY KEY,
    name varchar(25) UNIQUE NOT NULL,
    initiator_id integer REFERENCES users
);

CREATE TABLE tags_pictures (
    tag_id integer NOT NULL REFERENCES tags,
    pic_id integer NOT NULL REFERENCES pictures
);

# --- !Downs

DROP TABLE tags_pictures;
DROP TABLE tags;
DROP TABLE notations;
DROP TABLE pictures;
DROP TABLE users;
