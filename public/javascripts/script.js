
var $c = $("#review-canvas");
var ctx = $c[0].getContext("2d");
var img = new Image();
var notations = [];
const radius = 5;
img.addEventListener("load", function() {
  ctx.drawImage(img,0,0,$c[0].width,$c[0].height);
  var name = $(location).attr('pathname')
  console.log(name)
  $.get(name + "/notations", function (data) {
    console.log(data)
    notations = data.notations.reverse();
    notations.forEach(function (notation) {
      ctx.beginPath();
      ctx.arc(notation.xCoord, notation.yCoord, radius, 0, 2*Math.PI);
      ctx.stroke();
    });
  });
});
img.src = $c.data("image");

$c.click(function(ev) {
  var x = ev.offsetX;
  var y = ev.offsetY;
  var computed = getComputedStyle($c[0]),
      w_calc = parseInt(computed.width, 10),
      h_calc = parseInt(computed.height, 10);
  var h_canv = ctx.canvas.height;
  var w_canv = ctx.canvas.width;
  var h_factor = h_canv / h_calc;
  var w_factor = w_canv / w_calc;
  var x1 = parseInt(x * w_factor, 10);
  var y1 = parseInt(y * h_factor, 10);
  
  var touchedNotation = null
  notations.forEach( notation => {
    if (Math.pow(x1-notation.xCoord, 2) + Math.pow(y1-notation.yCoord, 2) <= Math.pow(radius, 2)) {
      touchedNotation = notation;
      return false;
    }
  });
  if (!touchedNotation) {
    $("#dialog-wrapper").show();
    $("input[name='x coordinate']").val(x1);
    $("input[name='y coordinate']").val(y1);
  } else {
    $("#review-show-notation").html(touchedNotation.notation).show();
  }
})

$("#dialog-overlay").click(function() {
  $("#dialog-wrapper").hide();
})
