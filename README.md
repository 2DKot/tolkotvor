# Tolkotvor #

This project is web blog where people can review some kind of art (picture, lyrics).

### How to install ###

* Install postgresql 9.6, sbt 0.13.13
* Create database 'tolkotvor_db' and user 'tolkotvor' with password
* Edit parameters "slick.dbs.default.db.PARAMETER" of config file conf/application.conf according to yours
* Run "sbt run" in terminal