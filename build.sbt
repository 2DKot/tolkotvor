name := """tolkotvor"""

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.11.8"

lazy val root = project.in(file(".")).enablePlugins(PlayScala)

resolvers += "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/"

resolvers := ("JBoss" at "https://repository.jboss.org/nexus/content/groups/public") +: resolvers.value

libraryDependencies ++= List(
  cache,
  "com.typesafe.play" %% "play-slick" % "2.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2",
  "org.postgresql" % "postgresql" % "9.4-1200-jdbc41",
  "com.github.nscala-time" %% "nscala-time" % "2.16.0",
  "org.typelevel" %% "cats" % "0.9.0",
  /*image processing*/
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.7",
  "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.7",
  "com.sksamuel.scrimage" %% "scrimage-filters" % "2.1.7"
)
