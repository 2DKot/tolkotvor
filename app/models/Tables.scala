
package models

import com.github.nscala_time.time.Imports._
import utils.ColumnTypeMappers.dateTimeMapper
import models.entities.{Notation, User, Picture, Tag => UserTag, TagPicture}
import slick.driver.PostgresDriver.api._

import javax.inject.Inject
/**
  * Created by hesowcharov on 26.01.17.
  */
object Tables {
  val notations = TableQuery[Notations]
  val pictures = TableQuery[Pictures]
  val users = TableQuery[Users]
  val tags = TableQuery[Tags]
  val tagsPictures = TableQuery[TagsPictures]

  class Users @Inject()(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Int]("user_id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def email = column[String]("email")
    def password = column[String]("password")
    def registered = column[DateTime]("registered")
    def * = (id.?, name, email.?, password, registered) <> (User.tupled, User.unapply)
  }

  class Pictures(tag: Tag) extends Table[Picture](tag, "pictures") {
    def id = column[Int]("pic_id", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("user_id")
    def width = column[Int]("width")
    def height = column[Int]("height")
    def format = column[String]("format", O.Length(10))
    def uniqueName = column[String]("unique_name", O.Length(20))
    def name = column[String]("name", O.Length(60))
    def description = column[String]("description")
    def uploaded = column[DateTime]("uploaded")
    def userFk = foreignKey("pictures_user_id_fkey", userId, users)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
    def * = (id.?, userId, width, height, format.?, uniqueName, name, description, uploaded) <> (Picture.tupled, Picture.unapply)
  }

  class Notations(tag: Tag) extends Table[Notation](tag, "notations") {
    import utils.ColumnTypeMappers.dateTimeMapper

    def id = column[Int]("notation_id", O.PrimaryKey, O.AutoInc)
    def pictureId = column[Int]("pic_id")
    def userId = column[Int]("user_id")
    def xCoord = column[Int]("xcoord")
    def yCoord = column[Int]("ycoord")
    def comment = column[String]("comment")
    def created = column[DateTime]("created")

    def pictureFK = foreignKey("notations_pic_id_fkey", pictureId, pictures)(
      _.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
    def userFK = foreignKey("notations_user_id_fkey", userId, users)(_.id, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.SetNull)

    def * = (id.?, pictureId, userId, xCoord, yCoord, comment, created) <> (Notation.tupled, Notation.unapply)
  }

  class Tags(tag: Tag) extends Table[UserTag](tag, "tags") {
    def id = column[Int]("tag_id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.Length(25))
    def initiatorId = column[Int]("initiator_id")

    def userFK = foreignKey("tags_initiator_id", initiatorId, users)(
      _.id, onDelete = ForeignKeyAction.SetNull)

    def * = (id.?, name, initiatorId) <> (UserTag.tupled, UserTag.unapply)
  }

  class TagsPictures(tag: Tag) extends Table[TagPicture](tag, "tags_pictures") {
    def tagId = column[Int]("tag_id")
    def pic_id = column[Int]("pic_id")

    def * = (tagId, pic_id) <> (TagPicture.tupled, TagPicture.unapply)
  }
}
