package models.entities

sealed trait AuthData

sealed case class Credentials(name: String, password: String) extends AuthData

sealed case class SessionID(id: String) extends AuthData
