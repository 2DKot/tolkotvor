package models.entities

import play.api.libs.json.Json
import com.github.nscala_time.time.Imports._

/**
  * Created by HesowcharovU on 13.10.2016.
  */

case class Notation(id: Option[Int], pictureID: Int, userID: Int, xCoord: Int, yCoord: Int, notation: String, created: DateTime) {
  implicit val notationFormat = Json.format[Notation]
}
