/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.19
 */

package models.entities

case class TagPicture(tagId: Int, pictureId: Int)
