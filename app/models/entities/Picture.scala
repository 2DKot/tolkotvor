package models.entities

import com.github.nscala_time.time.Imports._

/**
  * Created by hesowcharov on 23.01.17.
  */
case class Picture(id: Option[Int], uploaderId: Int, width: Int, height: Int, format: Option[String], uniqueName: String, name: String, description: String, uploaded: DateTime)
