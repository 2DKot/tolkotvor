package models

import scala.annotation.tailrec

/**
  * Created by HesowcharovU on 22.09.2016.
  */
class Lyrics(lines: Seq[Line])

//it is minimal fragment (fragment will be abstract class)
case class Word(that: String)

case class Line(words: List[Word]) {
  def range(from: Integer): Line = {
    require(from >= 1 && from <= words.length)
    Line(words.drop(from))
  }

  def range(from: Integer, to: Integer): Line = {
    @tailrec
    def go(counter: Integer, ws: List[Word], acc: List[Word]): List[Word] = {
      ws match {
        case word :: tail if (counter >= 0) => go(counter - 1, tail, word :: acc)
        case _ => acc
      }
    }

    require(to >= from && from >= 1 && to <= words.length)
    val counter = to - from
    val collectedWords = go(counter, words, List()).reverse
    Line(collectedWords)
  }

}