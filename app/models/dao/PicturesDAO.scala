package models.dao

import javax.inject.{Inject, Singleton}

import com.github.nscala_time.time.Imports._
import models.entities.Picture
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import models.Tables

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import utils.ColumnTypeMappers._

/**
  * Created by hesowcharov on 26.01.17.
  */


@Singleton
class PicturesDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends  HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.pictures

  // protected val insertSchema = pictures.map(p => (p.name, p.uploaded, p.userId))

  def add(picture: Picture): Future[Picture] = {
    val insertScheme = pictures returning pictures.map(_.id) into ((pic, newId) => pic.copy(id = Some(newId)))
    db.run(insertScheme += picture)
  }

  def getAll(): Future[Seq[Picture]] = db.run(pictures.to[Seq].result)

  def getByUniqueName(name: String): Future[Option[Picture]] = {
    val query = pictures
      .filter(_.uniqueName === name)
      .take(1)
      .result
    db.run(query)
      .map {seq => seq.headOption}
  }

  def getIdByUniqueName(name: String): Future[Option[Int]] = {
    for {
      optPic <- getByUniqueName(name)
    } yield optPic flatMap (_.id)
  }

}
