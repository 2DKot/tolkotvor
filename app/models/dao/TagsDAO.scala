/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.19
 */

package models.dao

import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import models.entities.{Tag, Picture, TagPicture}
import models.Tables

@Singleton
class TagsDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends HasDatabaseConfigProvider[JdbcProfile] {
  import Tables.{tags, tagsPictures}

  val insertQuery = tags returning tags.map(_.id) into
  ( (tag, newId) => tag.copy(id = Some(newId)) )

  val insertPicTagQuery = tagsPictures returning tagsPictures

  def add(tag: Tag): Future[Tag] = db.run(insertQuery += tag)

  def add(addingTags: Set[Tag]): Future[Seq[Tag]] = {
    val addingNames = addingTags.map(_.name)
    val existedTagsQuery = tags
      .filter(_.name inSetBind addingNames)
    for {
      existedTagsSeq <- db.run(existedTagsQuery.result)
      existedTagsSet = existedTagsSeq.toSet
      diff = addingTags diff existedTagsSet
      insertedTagsSeq <- db.run(insertQuery ++= diff  )
      insertedTagsSet = insertedTagsSeq.toSet
    } yield (existedTagsSet union insertedTagsSet).toSeq
  }

  def ref(picture: Picture, tags: Seq[Tag]): Future[Seq[TagPicture]] = {
    val data = tags.map { t => TagPicture(t.id.get, picture.id.get) }
    db.run(insertPicTagQuery ++= data)
  }
}
