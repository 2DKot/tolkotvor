package models.dao

import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import com.github.nscala_time.time.Imports._

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import cats._
import cats.data.EitherT
import cats.implicits._

import models.entities.{ User, Credentials }
import models.Tables
import utils.ColumnTypeMappers._
import utils.auth._

/**
  * Created by hesowcharov on 27.01.17.
  */

@Singleton
class UsersDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.users

  def getAll(): Future[Seq[User]] = db.run(users.result)

  def findBy(credentials: Credentials): EitherT[Future, AuthError, User] = {
    val query = for {
      user <- users if (user.name === credentials.name && user.password === credentials.password)
    } yield user

    EitherT.liftT(db.run(query.result.headOption)) flatMap { optUser =>
      optUser match {
        case Some(user) => EitherT.right(Future.successful(user))
        case None => EitherT.left(Future.successful(AuthError.UserNotFound))
      }
    }

  }

  def add(user: User): Future[User] = {
    val query = users returning users.map(_.id) into
      ( (user, createdId) => user.copy(id = Some(createdId)) )
    db.run(query += user)
  }

  def delete(user: User): Future[Unit] = {
    val query = for {
      deleting <- users if (deleting.id === user.id)
    } yield deleting
    db.run(query.delete).map(_ => ())
  }

}
