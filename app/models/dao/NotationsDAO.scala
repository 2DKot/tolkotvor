package models.dao

import javax.inject.{Inject, Singleton}

import models.entities.{Notation, User}
import models.Tables
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import com.github.nscala_time.time.Imports._

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 26.01.17.
  */

@Singleton
class NotationsDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider,
  protected val picturesDao: PicturesDAO)
    extends HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.{notations}

  def insert(essentialData: (Int, Int, String), pictureName: String, user: User): Future[Unit] = {
    val futPicId = picturesDao.getIdByUniqueName(pictureName)
    val futInsert = futPicId.flatMap { optPicId =>
      val (xCoord, yCoord, comment) = essentialData
      val created = DateTime.now
      val notation = Notation(None, optPicId.get, user.id.get, xCoord, yCoord, comment, created)
      db.run(notations += notation )
    }
    futInsert.map(_ => ())
  }

  def findByPictureName(name: String): Future[Seq[Notation]] = {
    val futPicId = picturesDao.getIdByUniqueName(name)
    futPicId.flatMap { picId =>
      val query = notations
        .filter(_.pictureId === picId)
        .to[Seq]
        .result
      db.run(query)
    }
  }

}
