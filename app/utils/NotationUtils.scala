/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import models.entities.Notation
import play.api.libs.json.Json

object NotationUtils {
  implicit val formats = Json.format[Notation]
}
