/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils.auth

import javax.inject.{Inject, Singleton}

import cats.data.EitherT
import cats.implicits._
import controllers.routes
import models.dao.UsersDAO
import models.entities.{Credentials, SessionID, User}
import play.api.cache._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import utils.{CacheStorage, Notification}

import scala.concurrent.Future
import scala.concurrent.duration._

@Singleton
class AuthComponentProvider @Inject() (
  val cache: CacheApi, 
  val usersDao: UsersDAO) {
}

trait AuthComponent extends AuthHandler {
  def provider: AuthComponentProvider
  def cache = provider.cache
  def usersDao = provider.usersDao

  case class UserRequest[A](user: Option[User],  notifications: Seq[Notification], request: Request[A])
      extends WrappedRequest[A](request)

  object UserAction extends ActionBuilder[UserRequest]
      with ActionTransformer[Request, UserRequest] {
    def transform[A](request: Request[A]) = Future.successful {
      val optUser = checkAuthorization(request)
      val nots = request.flash.data.toSeq.map { case (t, m) =>
        t match {
          case "error" => Notification.Error(m)
          case "notice" => Notification.Notice(m)
        }
      }
      UserRequest(optUser, nots, request)
    }
  }

  object CheckAuthorizationAction extends ActionFilter[UserRequest] {
    def filter[A](input: UserRequest[A]) = Future.successful {
      val UserRequest(optUser, _, _) = input
      optUser match {
        case Some(_) => None
        case _ => Some(
          Results.Redirect(routes.AuthController.signIn())
            .flashing("error" -> "You aren't authorized.")
        )
      }
    }
  }

  val AuthorizedAction = UserAction andThen CheckAuthorizationAction
}

trait AuthHandler
    extends AuthService
    with CacheStorage[User] {

  def expiryTime = 30 minutes

  def checkAuthorization(request: RequestHeader): Option[User] = {
    for {
      sessionID <- parseSessionID(request)
      user <- isAuthorized(sessionID)
    } yield user
  }

  def parseSessionID(request: RequestHeader): Option[SessionID] = {
    request.session
      .get("session_id")
      .map(SessionID(_))
  }
}

trait AuthService { this: CacheStorage[User]  =>
  def usersDao: UsersDAO

  def isAuthorized(sessionID: SessionID): Option[User] = {
    for {
      user <- getFromCache[User]("session." + sessionID.id)
    } yield user
  }

  def loginBy(credentials: Credentials): EitherT[Future, AuthError, SessionID] = {
    for {
      user <- usersDao.findBy(credentials)
    } yield {
      val uuid = UUID.generateUUID()
      setInCache(s"session.$uuid", user)
      SessionID(uuid)
    }
  }

  def logoutBy(sessionId: SessionID): Either[AuthError, Unit] = {
    val SessionID(id) = sessionId
    getFromCache(s"session.$id") match {
      case Some(_) => Either.right(removeFromCache(id))
      case _ => Either.left(AuthError.AuthorizationError("User wasn't logged in."))
    }
  }
}

