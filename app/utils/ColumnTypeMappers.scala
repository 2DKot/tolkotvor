/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import java.sql.Timestamp
import com.github.nscala_time.time.Imports.DateTime
import slick.driver.PostgresDriver.api._

object ColumnTypeMappers {
  implicit val dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      { dt => new Timestamp(dt.getMillis) }, { ts => new DateTime(ts.getTime) }
    )
}
