/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import play.api.cache._
import models.entities._

import scala.concurrent.duration.Duration
import scala.reflect.ClassTag

trait CacheStorage[A] {
  protected def cache: CacheApi

  protected def expiryTime: Duration

  def getFromCache[A: ClassTag](key: String): Option[A] = cache.get[A](key)

  def setInCache(key: String, user: A) = cache.set(key, user, expiryTime)

  def removeFromCache(key: String): Unit = cache.remove(key: String)
}

