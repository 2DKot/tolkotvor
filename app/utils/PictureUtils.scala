/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import models.entities.Picture
import play.api.libs.json.{Json, Reads, Writes}
import GeneralImplicits._
import play.api.data._
import play.api.data.Forms._


object PictureUtils {

  implicit val pictureWrites: Writes[Picture] = new Writes[Picture] {
    def writes(pic: Picture) = Json.obj(
      "name" -> pic.name,
      "uploaded" -> pic.uploaded
    )
  }

  val uploadPictureForm = Form(
    tuple(
      "name" -> nonEmptyText(1, 60),
      "description" -> text,
      "tags" -> text
    )
  )

  def parseTags(tags: String): Set[String] = {
     """\s*(\w+)\s*""".r
      .findAllIn(tags)
      .matchData
      .map(_.group(1))
      .toSet
  }
}
