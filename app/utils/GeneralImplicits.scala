/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import play.api.libs.json.{Reads, Writes}

object GeneralImplicits {
  implicit val dtReads = Reads.jodaDateReads("Y-M-d k:m:s")
  implicit val dtWrites = Writes.jodaDateWrites("Y-M-d k:m:s")
}
