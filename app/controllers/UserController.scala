package controllers

import javax.inject.Inject

import models.dao.UsersDAO
import models.entities.User
import utils.UserUtils._

import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 27.01.17.
  */
class UserController @Inject() (val messagesApi: MessagesApi,
                                val configuration: Configuration,
                                protected val usersDao: UsersDAO)
  extends ControllerWithConfig with I18nSupport {

  def getAllUsers = Action.async {
    import utils.UserUtils._
    usersDao.getAll().map { us => Ok(Json.toJson(us)) }
  }

  def addUser = Action.async(parse.json[User]) { implicit request =>
    val adding = request.body
    usersDao.add(adding).map(newUser => Ok(s"/users/${newUser.name}") )
  }

}
