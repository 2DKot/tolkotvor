package controllers

import javax.inject.{Inject, Singleton}

import models.dao.UsersDAO
import models.entities.{Credentials, SessionID}
import utils.{CredentialsUtils, UserUtils}
import utils.auth._

import play.api.Configuration
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc._
import play.api.cache._
import play.api.i18n.{I18nSupport, MessagesApi}

import cats._
import cats.data.{EitherT}
import cats.implicits._
import play.api.data.FormError

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 29.01.17.
  */

@Singleton
class AuthController @Inject() (
  val configuration: Configuration,
  override val usersDao: UsersDAO,
  override val cache: CacheApi,
  val provider: AuthComponentProvider,
  val messagesApi: MessagesApi
) extends ControllerWithConfig
    with AuthComponent
    with I18nSupport {

  def login = Action.async { implicit request =>

    def parseCredentials(request: Request[AnyContent])
                        (implicit req: Request[_]): EitherT[Future, AuthError, Credentials] = {
      CredentialsUtils.form.bindFromRequest.fold(
        formWithErrors => EitherT.left(
          Future.successful(AuthError.AuthenticateError("Form has error."))
        ),
        credentials => EitherT.right(
          Future.successful(credentials)
        )
      )
    }

    def checkLoggedIn(request: RequestHeader): EitherT[Future, AuthError, Unit] = {
      checkAuthorization(request) match {
        case Some(_) => EitherT.left(
          Future.successful(AuthError.AuthorizationError("User already logged in"))
        )
        case None => EitherT.right(Future.successful(()))
      }
    }

    val sessionId: EitherT[Future, AuthError, SessionID] = for {
      _ <- checkLoggedIn(request)
      creds <- parseCredentials(request)
      sessId <- loginBy(creds)
    } yield sessId

    sessionId.fold(
      err => {
        val msg = err match {
          case AuthError.AuthorizationError(msg) => msg
          case AuthError.AuthenticateError(msg) => msg
          case AuthError.UserNotFound => "Invalid credentials"
        }
        Redirect(routes.AuthController.signIn())
          .flashing("error" -> msg)
      },
      sessionId => {
        Redirect(routes.Application.index())
          .flashing("notice" -> "Authentication was successful")
          .withSession("session_id" -> sessionId.id)
      }
    )

  }

  def logout = UserAction { reqWithUser =>
    val UserRequest(optUser, _, request) = reqWithUser    
    val optLogout = for {
      session <- parseSessionID(request)
    } yield for {
      unit <- logoutBy(session)
    } yield unit

    optLogout match {
      case None => Unauthorized.flashing("error" -> "Logout status: no session_id")
      case Some(logoutRes) =>
        logoutRes.fold(
          authError => Unauthorized.flashing("error" -> "Logout status: user is not logged in (session_id was expired)"),
          success => Unauthorized.flashing("notice" -> "Logout status: user is logged out"))
    }
  }

  def register = Action.async(parse.form(UserUtils.registrationForm)) { implicit request =>
    val user = request.body
    for {
      _ <- usersDao.add(user)
    } yield Redirect(routes.Application.index()).flashing("notice" -> "Registration was successful")
  }

  def signIn = UserAction { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val page = views.html.main("Login page")(optUser, nots) {
      views.html.signin(CredentialsUtils.form)
    }
    Ok(page)
  }

  def signUp = UserAction { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val page = views.html.main("Registration page")(optUser, nots) {
      views.html.signup(UserUtils.registrationForm)
    }
    Ok(page)
  }

  def userProfile = AuthorizedAction { req =>
    val UserRequest(optUser, nots, request) = req
    val page = views.html.main("User profile")(optUser, nots) {
      views.html.profile(optUser)
    }
    Ok(page)
  }
}
