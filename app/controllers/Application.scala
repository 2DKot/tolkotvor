package controllers

import play.api._
import play.api.mvc._
import javax.inject.Inject

import play.api.i18n.{I18nSupport, MessagesApi}
import models.dao._
import utils.auth._

class Application @Inject() (val messagesApi: MessagesApi,
                             val configuration: Configuration,
                             protected val picturesDao: PicturesDAO,
                             protected val notationsDao: NotationsDAO,
                             override val usersDao: UsersDAO,
                             val provider: AuthComponentProvider)
    extends ControllerWithConfig with I18nSupport with AuthComponent {

  def index = UserAction { implicit request =>
    val UserRequest(optUser, nots, req) = request
    import views._
    val links = List(
      "/upload",
      "/pictures",
      "/signin",
      "/signup"
    )
    val page = html.main("Welcome!")(optUser, nots) {
      html.index(links)
    }
    Ok( page )
  }

}
