package controllers

import javax.inject.{Inject, Singleton}

import models.dao.NotationsDAO
import models.entities.Notation
import utils.auth.{AuthComponent, AuthComponentProvider}

import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc._

import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 27.01.17.
  */
@Singleton
class NotationController @Inject() (
  val messagesApi: MessagesApi,
  val configuration: Configuration,
  protected val notationsDao: NotationsDAO,
  val provider: AuthComponentProvider) extends ControllerWithConfig with I18nSupport with AuthComponent {

  def sendNotation(name: String) = AuthorizedAction.async(parse.form(models.forms.Repository.notationForm)) { implicit req =>
    val UserRequest(optUser, _, request) = req
    val notationData = request.body
    val afterInsert = notationsDao.insert(notationData, name, optUser.get)
    afterInsert.map { _ =>
      Redirect(routes.PictureController.showPicture(name))
    }
  }

  def notationsOf(picture: String) = Action.async { implicit request =>
    import utils.NotationUtils._

    notationsDao.findByPictureName(picture).map { seq =>
      val response = Json.obj(
        "status" -> "Ok",
        "notations" -> Json.toJson(seq))
      Ok(response)
    }
  }
}
