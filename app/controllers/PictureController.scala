package controllers

import javax.inject.Inject

import models.entities.{Picture, Tag => UserTag, TagPicture}
import models.dao.{PicturesDAO, TagsDAO}
import utils._
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc._
import models.entities.User
import utils.PictureUtils.uploadPictureForm
import play.api.cache._
import com.sksamuel.scrimage._
import com.sksamuel.scrimage.nio._

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import utils.auth.{AuthComponent, AuthComponentProvider}

/**
  * Created by hesowcharov on 27.01.17.
  */
class PictureController @Inject() (
  val messagesApi: MessagesApi,
  val configuration: Configuration,
  val picturesDao: PicturesDAO,
  val tagsDao: TagsDAO,
  override val cache: CacheApi,
  val provider: AuthComponentProvider
)   extends ControllerWithConfig
    with AuthComponent
    with CacheStorage[User]
    with I18nSupport {

  def uploadFileForm = AuthorizedAction { req =>
    val UserRequest(optUser, nots, request) = req
    val page = views.html.main("Upload new picture")(optUser, nots) {
      views.html.upload_file(uploadPictureForm)
    }
    Ok(page)
  }

  def uploadPicture = AuthorizedAction.async(parse.multipartFormData) { implicit reqWithUser =>
    import MultipartFormData.FilePart
    import play.api.libs.Files.TemporaryFile

    val UserRequest(optUser, _, request) = reqWithUser 
    val futImageFile = request.body.file("picture") match {
      case Some(picFile) =>
        picFile.contentType match {
          case Some(content) if (content.toLowerCase.contains("image")) => Future.successful(picFile)
          case _ => Future.failed(new IllegalArgumentException("no image"))
        }
      case _ => Future.failed(new IllegalArgumentException("no file"))
    }

    def handleForm = uploadPictureForm.bindFromRequest.fold(
      errForm => Future.failed(new Exception("Invalid filled form fields")),
      form => Future.successful(form)
    )

    def generateUniqueName(): Future[String] = {
      val uniqueName = scala.util.Random.alphanumeric.take(20).mkString("")
      picturesDao.getByUniqueName(uniqueName).flatMap { optPic =>
        optPic match {
          case None => Future.successful(uniqueName)
          case Some(_) => generateUniqueName
        }
      }
    }

    def moveFile(imageFile: FilePart[TemporaryFile], uniqueName: String) = Future {
      imageFile.ref.file.setReadable(true, false)
      val image = Image.fromFile(imageFile.ref.file)
      val resized = image.scaleTo(800, 600, ScaleMethod.Bicubic)
      val outputFile = new java.io.File(s"$mediaPath$uniqueName.jpg")
      resized.output(outputFile)(JpegWriter())
      image
    }

    def insertPictureData(name: String, description: String, uniqueName: String, image: Image): Future[Picture] = {
      import com.github.nscala_time.time.Imports._

      val (width, height) = image.dimensions
      val format = FormatDetector.detect(image.stream).flatMap { format =>
        format match {
          case Format.JPEG => Some("jpg")
          case Format.PNG => Some("png")
          case Format.GIF => Some("gif")
          case _ => None
        }
      }
      val picture = Picture(None, optUser.get.id.get, width, height, format, uniqueName, name, description, DateTime.now)
      picturesDao.add(picture)
    }

    def insertTagsData(tagsText: String): Future[Seq[UserTag]] = {
      val tags = PictureUtils
        .parseTags(tagsText)
        .map(t => UserTag(None, t, optUser.get.id.get))
      tagsDao.add(tags)
    }

    def insertTagPictureData(pic: Picture, tags: Seq[UserTag]): Future[Seq[TagPicture]] = {
      tagsDao.ref(pic, tags)
    }

    val answer = for {
      (name, description, tagsText) <- handleForm
      file <- futImageFile
      uniqueName <- generateUniqueName()
      image <- moveFile(file, uniqueName)
      picture <- insertPictureData(name, description, uniqueName, image)
      tags <- insertTagsData(tagsText)
      tagsPicture <- insertTagPictureData(picture, tags)
    } yield {
      Redirect(routes.PictureController.showPicture(picture.uniqueName))
    }

    answer.recover {
      case exc: IllegalArgumentException =>
        Redirect(routes.PictureController.uploadFileForm())
          .flashing("error" -> exc.getMessage)
      case exc: Exception =>
        Redirect(routes.PictureController.uploadFileForm())
          .flashing("error" -> exc.getMessage)
    }
  }


  def showPicture(uniqueName: String) = UserAction.async {  req =>
    val UserRequest(optUser, nots, _) = req
    val futOptPic = picturesDao.getByUniqueName(uniqueName)
    futOptPic map { optPic =>
      optPic.fold(
        Redirect(routes.PictureController.showAllPictures())
          .flashing("error" -> "Picture doesn't exist")) ( picture => {
        val page = views.html.main(s"Review of ${picture.name}")(optUser, nots) {
          views.html.picture_review(staticRoute, picture)
        }
        Ok(page)
      })
    }
  }

  def getAll = Action.async {
    import utils.PictureUtils._

    val futPics = picturesDao.getAll()
    futPics.map { pics: Seq[Picture] =>
      Ok(Json.toJson(pics))
    }
  }

  def showAllPictures = UserAction.async { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val mainPage = views.html.main(s"All pictures of Tolkotvor")(optUser, nots)_
    val futPics = picturesDao.getAll()
    futPics map { pics =>
      Ok( mainPage(views.html.pictures(pics, staticRoute)) )
    }
  }

}
