package controllers

import play.api.Configuration
import play.api.mvc.Controller

/**
  * Created by hesowcharov on 27.01.17.
  */
trait ControllerWithConfig extends Controller {
  val configuration: Configuration
  require(
    configuration.getString("application.media.path").isDefined,
    "Path to media dir doesn't defined in application.conf (application.media.path)"
  )
  require(
    configuration.getString("application.static.route").isDefined,
    "Route to static files doesn't defined in application.conf (application.static.route)"
  )

  protected val mediaPath = configuration.getString("application.media.path").get
  protected val staticRoute = configuration.getString("application.static.route").get
}
